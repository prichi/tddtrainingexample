package com.iexpertos.encriptor;

import java.security.InvalidParameterException;

public class Encryptor {

	private static final String WORD_SEPERATOR = " ";

	public String cryptWord(String word) {
		return cryptWord(word, word);
	}

	public String cryptWordToNumbers(String word) {
		String result = "";
		
		for (int character : cryptWord(word).toCharArray()) {
			result += String.valueOf(character);
		}

		return result;
	}
	

	private void validatesForBlank(String word) {
		if (word.contains(WORD_SEPERATOR))
			throw new InvalidParameterException();
	}

	public String cryptWord(String word, String charsToReplace) {
		validatesForBlank(word);

		char[] wordArray = word.toCharArray();

		for (int i = 0; i < wordArray.length; i++) {
			wordArray[i] = cryptSingleOnlyReplacementMatches(wordArray[i], charsToReplace);
		}

		return String.valueOf(wordArray);
	}

	private char cryptSingleOnlyReplacementMatches(char character, String replacementMatches) {
		char[] replacement = replacementMatches.toCharArray();

		for (int j = 0; j < replacement.length; j++) {
			if (replacement[j] == character) {
				return cryptSingleCharacter(character);
			}
		}

		return character;
	}

	private char cryptSingleCharacter(int charValue) {
		return (char) (charValue + 2);
	}

	public String cryptSentence(String sentence) {
		String newWord = "";

		for (char character : sentence.toCharArray()) {
			newWord += String.valueOf(cryptSingleCharacter(character));
		}

		return newWord;
	}

	public String[] getWords(String sentence) {
		return sentence.split(WORD_SEPERATOR);
	}

	public void printWords(String sentence) {
		String[] words = getWords(sentence);
		for (String word : words) {
			System.out.print("<" + word + ">");
		}
	}

}