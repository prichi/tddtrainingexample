package com.iexpertos.encriptor.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.InvalidParameterException;

import org.junit.Before;
import org.junit.Test;

import com.iexpertos.encriptor.Encryptor;

public class EncryptorTest {

	private Encryptor encryptor;

	private static final String TEST_SENTENCE = "Das ist unser Testsatz!!!";

	@Before
	public void init() {
		encryptor = new Encryptor();
	}

	@Test(expected = InvalidParameterException.class)
	public void cryptWord_InputWithBlank_throwsInvalidParameterException() {
		encryptor.cryptWord("  ");
	}

	@Test
	public void cryptWord_InputAABB_returnsCCDD() {
		String cryptWordResult = encryptor.cryptWord("AABB");
		
		assertEquals("CCDD", cryptWordResult);
	}

	@Test
	public void cryptWord_InputSpecialChar_returnsSpecialChars() {
		String cryptWordResult = encryptor.cryptWord("ßöü");
		
		assertEquals("áøþ", cryptWordResult);
	}

	@Test(expected = InvalidParameterException.class)
	public void cryptWordToNumbers_InputWithBlank_throwsInvalidParameterException() {
		encryptor.cryptWordToNumbers("  ");
	}

	@Test
	public void cryptWordToNumbers_InputAABB_returnsCCDD() {
		String cryptWordResult = encryptor.cryptWordToNumbers("AABB");
		
		assertEquals("67676868", cryptWordResult);
	}

	@Test
	public void cryptWordToNumbers_InputSpecialChar_returnsSpecialChars() {
		String cryptWordResult = encryptor.cryptWordToNumbers("ßöü");
		
		assertEquals("225248254", cryptWordResult);
	}

	@Test(expected = InvalidParameterException.class)
	public void cryptWordReplace_InputBlank_throwsInvalidParameterException() {
		encryptor.cryptWord(" ", null);
	}

	@Test
	public void cryptWordReplace_InputAABBCC_returnsAADDCC() {
		String cryptWordResult = encryptor.cryptWord("AABBCC", "B");
		
		assertEquals("AADDCC", cryptWordResult);
	}

	@Test
	public void cryptWordReplace_InputSpecialChar_returnsSpecialChars() {
		String cryptWordResult = encryptor.cryptWord("AABBCC", "B");
		
		assertEquals("AADDCC", cryptWordResult);
	}

	@Test
	public void cryptSentence_InputTestSentenceWithSpecialChars_returnsShiftedTestSentence() {
		String cryptWordResult = encryptor.cryptSentence(TEST_SENTENCE);
		
		assertEquals("Fcu\"kuv\"wpugt\"Vguvucv|###", cryptWordResult);
	}

	@Test
	public void getWords_InputTestSentenceWithSpecialChars_returnsSplittedTestSentence() {
		String[] cryptWordResult = encryptor.getWords(TEST_SENTENCE);
		
		assertEquals("Das", cryptWordResult[0]);
		assertEquals("ist", cryptWordResult[1]);
		assertEquals("unser", cryptWordResult[2]);
		assertEquals("Testsatz!!!", cryptWordResult[3]);
	}

	@Test
	public void printWords_InputTestSentenceWithSpecialChars_returnsSplittedTestSentence() {
		ByteArrayOutputStream bos = mockSystemOut();
		
		encryptor.printWords(TEST_SENTENCE);

		assertEquals("<Das><ist><unser><Testsatz!!!>", bos.toString());
	}

	private ByteArrayOutputStream mockSystemOut() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);

		PrintStream printStream = new PrintStream(bos);
		System.setOut(printStream);
		return bos;
	}

	// @Test
	// public void cryptWord_InputSpecialChar_returnsSpecialChars() {
	// String cryptWordResult = encryptor.cryptWord("���");
	// assertEquals("���", cryptWordResult);
	// }

}
